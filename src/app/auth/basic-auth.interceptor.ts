import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable()
export class BasicAuthInterceptor implements HttpInterceptor {

  private username = '40c9b7d5cb0ecb9abaab';
  private password = '76c5d1b201c6d52ceae63188fe6d06';

  constructor() { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const basicAuth = `${this.username}:${this.password}`;

    request = request.clone({
      setHeaders: {
        Authorization: `Basic ${btoa(basicAuth)}`
      }
    });

    return next.handle(request);
  }
}
