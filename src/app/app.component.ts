import { Component } from '@angular/core';

@Component({
  selector: 'lup-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  showNavigation = true;

  onToggleNavigation(): void {
    this.showNavigation = !this.showNavigation;
  }
}
