export interface NavigationLink {
  routerLink: string;
  iconClasses: string;
  disabledLink?: boolean;
}
