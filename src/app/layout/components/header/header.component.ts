import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'lup-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  @Output() toggleNavigation: EventEmitter<void> = new EventEmitter();

  onHamburgerClick(): void {
    this.toggleNavigation.emit();
  }
}
