import { Component, OnInit } from '@angular/core';
import { NavigationLink } from 'src/app/layout/model/navigation-link';
import { LayoutService } from 'src/app/layout/services/layout.service';

@Component({
  selector: 'lup-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  links: NavigationLink[];

  constructor(private layoutService: LayoutService) {

  }

  ngOnInit(): void {
    this.links = this.layoutService.getNavigationLinks();
  }

}
