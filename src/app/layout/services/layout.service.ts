import { Injectable } from '@angular/core';
import { NavigationLink } from 'src/app/layout/model/navigation-link';

@Injectable()
export class LayoutService {

  public getNavigationLinks(): NavigationLink[] {

    return [
      {
        routerLink: '/dashboard',
        iconClasses: 'bi bi-speedometer2'
      },
      {
        routerLink: '/new-route',
        iconClasses: 'bi bi-journal-text',
        disabledLink: true
      },
      {
        routerLink: '/new-route',
        iconClasses: 'bi bi-folder',
        disabledLink: true
      },
      {
        routerLink: '/new-route',
        iconClasses: 'bi bi-file-earmark-text',
        disabledLink: true
      },
      {
        routerLink: '/new-route',
        iconClasses: 'bi bi-chat',
        disabledLink: true
      },
      {
        routerLink: '/new-route',
        iconClasses: 'bi bi-cart',
        disabledLink: true
      },
      {
        routerLink: '/users',
        iconClasses: 'bi bi-person'
      },
      {
        routerLink: '/new-route',
        iconClasses: 'bi bi-people',
        disabledLink: true
      },
      {
        routerLink: '/new-route',
        iconClasses: 'bi bi-arrow-left-right',
        disabledLink: true
      },
      {
        routerLink: '/new-route',
        iconClasses: 'bi bi-bezier',
        disabledLink: true
      },
      {
        routerLink: '/new-route',
        iconClasses: 'bi bi-house',
        disabledLink: true
      },
      {
        routerLink: '/new-route',
        iconClasses: 'bi bi-circle',
        disabledLink: true
      },
      {
        routerLink: '/new-route',
        iconClasses: 'bi bi-pie-chart',
        disabledLink: true
      },
      {
        routerLink: '/new-route',
        iconClasses: 'bi bi-gear',
        disabledLink: true
      }
    ];
  }

}
