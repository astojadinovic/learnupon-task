import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { FormlyModule } from '@ngx-formly/core';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CreateUserModalComponent } from 'src/app/users/components/create-user-modal/create-user-modal.component';
import { UsersTableComponent } from 'src/app/users/components/users-table/users-table.component';
import { UsersListComponent } from 'src/app/users/containers/users-list/users-list.component';
import { UsersService } from 'src/app/users/services/users.service';
import { UsersRoutingModule } from 'src/app/users/users-routing.module';

@NgModule({
  declarations: [
    UsersListComponent,
    UsersTableComponent,
    CreateUserModalComponent
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    ModalModule.forChild(),
    ReactiveFormsModule,
    FormlyModule.forChild({ extras: { lazyRender: true } }),
    FormlyBootstrapModule
  ],
  providers: [
    UsersService
  ]
})
export class UsersModule { }
