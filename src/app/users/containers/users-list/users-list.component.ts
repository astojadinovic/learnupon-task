import { Component, OnDestroy, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CreateUserModalComponent } from 'src/app/users/components/create-user-modal/create-user-modal.component';
import { CreateUserCmd } from 'src/app/users/model/create-user-cmd';
import { User } from 'src/app/users/model/user';
import { UsersResponse } from 'src/app/users/model/users-response';
import { UsersService } from 'src/app/users/services/users.service';

@Component({
  selector: 'lup-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit, OnDestroy {

  users: User[];

  private destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private usersService: UsersService,
    private modalService: BsModalService
  ) {

  }

  async ngOnInit(): Promise<void> {
    const usersResponse: UsersResponse = await this.usersService.getUsers();
    this.users = usersResponse?.user;
  }

  onCreate(): void {
    const modalRef = this.modalService.show(CreateUserModalComponent);
    const modalContent: CreateUserModalComponent = modalRef.content as CreateUserModalComponent;

    modalContent.onClose
      .pipe(takeUntil(this.destroy$))
      .subscribe((result: User) => {
        if (!!result) {

          const createUserCmd: CreateUserCmd = {
            user: result
          };
          this.usersService.createUser(createUserCmd).then((response) => {

          }).catch(() => {
          // create don't work (as stated in the task text), so I'm just mocking some response data
            const response = Object.assign({
              id: new Date().getUTCMilliseconds(),
              number_of_enrollments: 3,
              number_of_enrollments_accessed: 4,
              sign_in_count: 5
            }, result);

            this.users.push(response);
            modalContent.bsModalRef.hide();
          });
        }
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

}
