import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { User } from 'src/app/users/model/user';


@Component({
  selector: 'lup-create-user-modal',
  templateUrl: './create-user-modal.component.html',
  styleUrls: ['./create-user-modal.component.scss']
})
export class CreateUserModalComponent implements OnInit {

  public onClose: Subject<User | boolean>;

  form: FormGroup;
  fields: FormlyFieldConfig[];

  constructor(public bsModalRef: BsModalRef) {

  }

  ngOnInit(): void {
    this.onClose = new Subject();
    this.createForm();
  }

  close(): void {
    this.onClose.next(false);
    this.bsModalRef.hide();
  }

  onSubmit(): void {
    this.onClose.next(this.form.getRawValue());
  }

  createForm(): void {
    this.form = new FormGroup({});
    this.fields = [
      {
        key: 'first_name',
        type: 'input',
        templateOptions: {
          label: 'First name',
          placeholder: 'Enter first name',
          required: true,
        }
      },
      {
        key: 'last_name',
        type: 'input',
        templateOptions: {
          label: 'Last name',
          placeholder: 'Enter last name',
          required: true,
        }
      },
      {
        key: 'email',
        type: 'input',
        templateOptions: {
          label: 'Email address',
          placeholder: 'Enter email',
          required: true,
        },
        validators: {
          validEmail: {
            expression: (c) => /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(c.value),
            message: (error, field: FormlyFieldConfig) => `"${field.formControl.value}" is not a valid email address`,
          },
        },
      }
    ];
  }



}
