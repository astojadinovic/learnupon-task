import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from 'src/app/users/model/user';

@Component({
  selector: 'lup-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.scss']
})
export class UsersTableComponent implements OnInit {

  @Input() users: User[];
  @Output() create: EventEmitter<void> = new EventEmitter();

  constructor() {

  }

  ngOnInit(): void {

  }

  identify(index: number, item: User): number {
    return item?.id;
  }

  onCreate(): void {
    this.create.emit();
  }

}
