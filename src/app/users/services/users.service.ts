import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CreateUserCmd } from 'src/app/users/model/create-user-cmd';
import { UsersResponse } from 'src/app/users/model/users-response';
import { handleErrorResultResponse } from 'src/app/util/error-util';

@Injectable()
export class UsersService {

  private baseUserUrl = 'api/v1/users';

  constructor(private http: HttpClient) {

  }

  public getUsers(): Promise<UsersResponse> {

    return this.http
      .get(this.baseUserUrl)
      .pipe(
        catchError(handleErrorResultResponse)
      )
      .toPromise();
  }

  public createUser(createUserCmd: CreateUserCmd): Promise<UsersResponse> {

    return this.http
      .post(this.baseUserUrl, createUserCmd)
      .pipe(
        catchError(handleErrorResultResponse)
      )
      .toPromise();
  }

}
