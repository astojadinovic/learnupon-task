import { User } from 'src/app/users/model/user';

export interface UsersResponse {
  user?: Array<User>;
}
