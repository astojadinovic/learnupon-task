import { User } from 'src/app/users/model/user';

export interface CreateUserCmd {
  user: User;
}
