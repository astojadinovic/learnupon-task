
export interface User {
  CustomData?: any;
  account_expires?: any;
  can_delete_users?: boolean;
  can_enroll?: boolean;
  can_mark_complete?: boolean;
  can_move_groups?: boolean;
  can_unenroll_users?: boolean;
  created_at?: Date;
  customDataFieldValues?: Array<any>;
  email?: string;
  enabled?: boolean;
  first_name?: string;
  id?: number;
  is_salesforce_contact?: number;
  last_name?: string;
  last_sign_in_at?: Date;
  locale?: string;
  number_of_enrollments?: number;
  number_of_enrollments_accessed?: number;
  sf_contact_id?: any;
  sf_user_id?: any;
  sign_in_count?: number;
  tutor_can_create_courses?: boolean;
  tutor_can_edit_their_courses?: boolean;
  user_type?: string;
}
